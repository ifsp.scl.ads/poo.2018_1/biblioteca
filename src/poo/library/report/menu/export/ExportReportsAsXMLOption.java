package poo.library.report.menu.export;

import poo.library.common.view.OptionViewModel;
import poo.library.report.ReportInteractor;

import java.util.Scanner;

public class ExportReportsAsXMLOption implements OptionViewModel {
    @Override
    public String getTitle() {
        return "XML";
    }

    @Override
    public void execute(Scanner scanner) {
        ReportInteractor.exportReportToXMLFile();
        System.out.println("Arquivos exportados com sucesso");
    }
}
