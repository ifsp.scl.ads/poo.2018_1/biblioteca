package poo.library.report.menu.export;

import poo.library.common.view.OptionViewModel;
import poo.library.report.ReportInteractor;

import java.util.Scanner;

public class ExportReportsAsCSVOption implements OptionViewModel {
    @Override
    public String getTitle() {
        return "CSV";
    }

    @Override
    public void execute(Scanner scanner) {
        ReportInteractor.exportReportToCSVFile();
        System.out.println("Arquivos exportados com sucesso");
    }
}
