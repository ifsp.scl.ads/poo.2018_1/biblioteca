package poo.library.report.menu;

import poo.library.common.view.OptionViewModel;
import poo.library.report.menu.export.ReportExportPresenter;

import java.util.Scanner;

public class GenerateReportOption implements OptionViewModel {

	@Override
	public String getTitle() {
		return "Gerar relatórios";
	}

	@Override
	public void execute(Scanner scanner) {
        new ReportExportPresenter().present(scanner);
	}
}
