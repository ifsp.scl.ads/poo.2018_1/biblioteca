package poo.library.report.exporter;

import poo.library.domain.penalty.PenaltyInteractor;
import poo.library.report.formatter.Formatter;

class PenaltyReportExporter extends ReportExporter {
    @Override
    protected void exportReport(Formatter formatter) {
        exportToFileWithFormatter(PenaltyInteractor.getAllPenalties(), formatter, "PenaltyReport");
    }
}
