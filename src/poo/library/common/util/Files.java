package poo.library.common.util;

import java.io.*;

public final class Files {
    private Files() {

    }

    public static void saveStringToFile(String content, String filename) {
        Writer writer = null;
        try {
            writer = new BufferedWriter(
                    new OutputStreamWriter(
                            new FileOutputStream(filename)
                    )
            );
            writer.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
