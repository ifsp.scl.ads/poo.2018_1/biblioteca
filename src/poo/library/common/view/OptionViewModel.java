package poo.library.common.view;

import java.util.Scanner;

public interface OptionViewModel {
    String getTitle();
    void execute(Scanner scanner);
}
