package poo.library.domain.book.view.menu.search;

import poo.library.common.view.OptionViewModel;
import poo.library.common.view.Presenter;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class BookSearchPresenter extends Presenter {
    private static List<OptionViewModel> options;
    static {
        options = Arrays.asList(
                new ConsultaLivroTitulo(),
                new ConsultaLivroAutor(),
                new ConsultaLivroCategoria(),
                new ConsultaLivroDisponibilidade()
        );
    }

    @Override
    protected List<OptionViewModel> options() {
        return options;
    }

    @Override
    public void present(Scanner scanner) {
        System.out.println("Consultar livros por:");
        super.present(scanner);
    }
}
