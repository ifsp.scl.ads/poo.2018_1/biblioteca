package poo.library.domain.book.view.menu;

import poo.library.common.view.OptionViewModel;
import poo.library.domain.book.BookInteractor;

import java.util.Scanner;

public class CadastroLivro implements OptionViewModel {

	@Override
	public String getTitle() {
		return "Cadastrar livro";
	}

	@Override
	public void execute(Scanner entrada) {
        int codigo;
		String autor;
		String categoria;
        String nome;
		int ano;
		int prioridade;
		int disponibilidade;
		
		System.out.println("**************************************************");
		System.out.println("Cadastro de livro:");
		System.out.println("**************************************************");

        System.out.println("Informe o codigo do livro: ");
        codigo = entrada.nextInt();

		String cache = entrada.nextLine();//consome o buffer deixado pelo Enter para corrigiro o problema de buffer de entrada

		System.out.println("Informe o título do livro: ");
		nome = entrada.nextLine();

		System.out.println("Informe o autor do Livro: ");
		autor = entrada.nextLine();

		System.out.println("Informe a categoria do Livro: ");
		categoria = entrada.nextLine();

		System.out.println("Informe o Ano da Edi��o do Livro: ");
		ano = entrada.nextInt();

		System.out.println("Informe o nivel de prioridade do Livro: ");
		prioridade = entrada.nextInt();

		disponibilidade = 1;

        if (BookInteractor.getBookById(codigo) == null) {
            BookInteractor.saveBook(codigo, nome, autor, categoria, ano, prioridade, disponibilidade);
            System.out.println("**********Cadastrado com Sucesso!**************");

        } else {
            System.out.println("**********Erro: Código já cadastrado**************");
        }
	}

}
