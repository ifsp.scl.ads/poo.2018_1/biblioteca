/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.library.domain.user.model;

import poo.library.common.model.Mapable;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ghonorio
 */
public class Usuario implements Mapable {
    
    private int id;
    private String nome;
    private String categoria;
    private String endereco;
    private String telefone;
    private char sexo;

    public Usuario(int id, String nome, String categoria, String endereco, String telefone, char sexo) {
        this.id = id;
        this.nome = nome;
        this.sexo = sexo;
        this.categoria = categoria;
        this.endereco = endereco;
        this.telefone = telefone;
    }
    
    public void setId(int id){
        this.id=id;
    }
    
    public int getId(){
        return id;
    }
    
    public void setNome(String nome){
        this.nome=nome;
    }
    
    public String getNome(){
        return nome;
    }
    
    public void setSexo(char sexo){
        this.sexo=sexo;
    }
    
    public char getSexo(){
        return sexo;
    }
    
    public void setCategoria(String categoria){
        this.categoria=categoria;
    }
    
    public String getCategoria(){
        return categoria;
    }
    
    public void setEndereco(String endereco){
        this.endereco=endereco;
    }
    
    public String getEndereco(){
        return endereco;
    }
    
    public void setTelefone(String telefone){
        this.telefone=telefone;
    }
    
    public String getTelefone(){
        return telefone;
    }

    @Override
    public Map<String, Object> mappedProperties() {
        Map<String, Object> properties = new HashMap<>();

        properties.put("id", id);
        properties.put("nome", nome);
        properties.put("categoria", categoria);
        properties.put("endereco", endereco);
        properties.put("telefone", telefone);
        properties.put("sexo", sexo);

        return properties;
    }
}
