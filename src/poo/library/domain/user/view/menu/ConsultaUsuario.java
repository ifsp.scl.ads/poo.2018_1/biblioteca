package poo.library.domain.user.view.menu;

import poo.library.common.view.OptionViewModel;
import poo.library.domain.user.UserInteractor;
import poo.library.domain.user.view.UserListingUtil;

import java.util.Scanner;

public class ConsultaUsuario implements OptionViewModel {

	@Override
	public String getTitle() {
		return "Pesquisar usuários";
	}

	@Override
	public void execute(Scanner entrada) {
		String nome;

		System.out.println("********************************************************");
		System.out.println("Consulta de usuário:");
		System.out.println("********************************************************");

		String cache = entrada.nextLine();//consome o buffer deixado pelo Enter para corrigiro o problema de buffer de entrada

        System.out.println("Informe o nome do usuário desejado: ");
		nome = entrada.nextLine();

		UserListingUtil.list(UserInteractor.getUsersFilteredByName(nome));
	}

}
