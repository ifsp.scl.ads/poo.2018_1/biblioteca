package poo.library.domain.user.view;

import poo.library.domain.user.model.Usuario;

import java.util.List;

public final class UserListingUtil {
    private UserListingUtil() {

    }

	public static void list(List<Usuario> usuarios) {
		for (Usuario usuario : usuarios) {
            System.out.println("--------------------------");
			System.out.println("ID: " + usuario.getId());
			System.out.println("Nome: " + usuario.getNome());
			System.out.println("Categoria: " + usuario.getCategoria());
            System.out.println("Endereço: " + usuario.getEndereco());
            System.out.println("Telefone: " + usuario.getTelefone());
            System.out.println("Gênero: " + usuario.getSexo());
			System.out.println("--------------------------");
		}
	}
}