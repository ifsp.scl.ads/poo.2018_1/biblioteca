package poo.library.main.presenter.librarian.menu;

import poo.library.common.view.OptionViewModel;
import poo.library.common.view.Presenter;
import poo.library.domain.loan.menu.CadastroEmprestimo;
import poo.library.domain.loan.menu.ConsultaEmprestimo;

import java.util.Arrays;
import java.util.List;

public class LoanManagementPresenter extends Presenter {
    private static List<OptionViewModel> options;
    static {
        options = Arrays.asList(
                new CadastroEmprestimo(),
                new ConsultaEmprestimo()
        );
    }

    @Override
    protected List<OptionViewModel> options() {
        return options;
    }
}
