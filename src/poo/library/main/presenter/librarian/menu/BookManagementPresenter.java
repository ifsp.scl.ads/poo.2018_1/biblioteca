package poo.library.main.presenter.librarian.menu;

import poo.library.common.view.OptionViewModel;
import poo.library.common.view.Presenter;
import poo.library.domain.book.view.menu.AlteraLivro;
import poo.library.domain.book.view.menu.BookSearchOption;
import poo.library.domain.book.view.menu.CadastroLivro;
import poo.library.domain.book.view.menu.RemoveLivro;

import java.util.Arrays;
import java.util.List;

public class BookManagementPresenter extends Presenter {
    private static List<OptionViewModel> options;
    static {
        options = Arrays.asList(
                new CadastroLivro(),
                new AlteraLivro(),
                new RemoveLivro(),
                new BookSearchOption()
        );
    }

    @Override
    protected List<OptionViewModel> options() {
        return options;
    }
}
