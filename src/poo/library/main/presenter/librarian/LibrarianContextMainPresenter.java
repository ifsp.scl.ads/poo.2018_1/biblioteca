package poo.library.main.presenter.librarian;

import poo.library.common.view.OptionViewModel;
import poo.library.domain.penalty.menu.CadastroMulta;
import poo.library.main.presenter.MainPresenter;
import poo.library.main.presenter.librarian.menu.BookManagementOption;
import poo.library.main.presenter.librarian.menu.LoanManagementOption;
import poo.library.main.presenter.librarian.menu.UserManagementOption;
import poo.library.report.menu.GenerateReportOption;

import java.util.Arrays;
import java.util.List;

public class LibrarianContextMainPresenter extends MainPresenter {
    private static List<OptionViewModel> options;
    static {
        options = Arrays.asList(
                new BookManagementOption(),
                new UserManagementOption(),
                new LoanManagementOption(),
                new CadastroMulta(),
                new GenerateReportOption()
        );
    }

    @Override
    protected List<OptionViewModel> options() {
        return options;
    }
}
